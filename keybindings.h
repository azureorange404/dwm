static Keychord *keychords[] = {
	/* modifier                     key        function        argument */
	&((Keychord){1, {{ MODKEY,                           XK_Return}},                   spawn,                 {.v = termcmd } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_Return}},                   spawn,                 {.v = dmenucmd } }),
	&((Keychord){1, {{ MODKEY,                           XK_r}},                        spawn,                 {.v = dmenucmd } }),
	&((Keychord){1, {{ MODKEY|ALTKEY,                    XK_l}},                        spawn,                 {.v = quitcmd } }),
	&((Keychord){1, {{ ALTKEY,                           XK_l}},                        spawn,                 SHCMD("slock") }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_w}},                        spawn,                 {.v = chngebg } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_space}},                    spawn,                 {.v = tgltray } }),
	&((Keychord){1, {{ MODKEY,                           XK_Escape}},                   togglebar,             {0} }),
	&((Keychord){1, {{ MODKEY|ShiftMask|ControlMask,     XK_i}},                        incnmaster,            {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask|ControlMask,     XK_d}},                        incnmaster,            {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ALTKEY|ControlMask,        XK_h}},                        setmfact,              {.f = -0.05} }),
	&((Keychord){1, {{ MODKEY|ALTKEY|ControlMask,        XK_l}},                        setmfact,              {.f = +0.05} }),
	&((Keychord){1, {{ MODKEY,                           XK_BackSpace}},                zoom,                  {0} }),
	&((Keychord){1, {{ MODKEY,                           XK_q}},                        killclient,            {0} }),
	&((Keychord){1, {{ MODKEY,                           XK_space}},                    togglefullscr,         {0} }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_f}},                        togglefloating,        {0} }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_t}},                        setlayout,             {.v = &layouts[0]} }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_f}},                        setlayout,             {.v = &layouts[1]} }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_m}},                        setlayout,             {.v = &layouts[2]} }),

	&((Keychord){1, {{ MODKEY,                           XK_comma}},                    focusmon,              {.i = -1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_period}},                   focusmon,              {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_comma}},                    tagmon,                {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_period}},                   tagmon,                {.i = +1 } }),

	&((Keychord){1, {{ MODKEY,                           XK_Left}},                     shiftviewclients,      {.i = -1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_Right}},                    shiftviewclients,      {.i = +1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_h}},                        shiftviewclients,      {.i = -1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_l}},                        shiftviewclients,      {.i = +1 } }),

	&((Keychord){1, {{ MODKEY|ControlMask,               XK_Left}},                     newshiftview,          {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_Right}},                    newshiftview,          {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_h}},                        newshiftview,          {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ControlMask,               XK_l}},                        newshiftview,          {.i = +1 } }),

	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_Left}},                     shiftboth,             {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_Right}},                    shiftboth,             {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_h}},                        shiftboth,             {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_l}},                        shiftboth,             {.i = +1 } }),

	&((Keychord){1, {{ MODKEY,                           XK_Down}},                     focusstack,            {.i = +1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_Up}},                       focusstack,            {.i = -1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_j}},                        focusstack,            {.i = +1 } }),
	&((Keychord){1, {{ MODKEY,                           XK_k}},                        focusstack,            {.i = -1 } }),

	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_Down}},                     movestack,             {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_Up}},                       movestack,             {.i = -1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_j}},                        movestack,             {.i = +1 } }),
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_k}},                        movestack,             {.i = -1 } }),

	&((Keychord){1, {{ 0,                                XF86XK_AudioLowerVolume}},     spawn,                 {.v = decrvol } }),
	&((Keychord){1, {{ 0,                                XF86XK_AudioRaiseVolume}},     spawn,                 {.v = incrvol } }),
	&((Keychord){1, {{ 0,                                XF86XK_AudioMute}},            spawn,                 {.v = mutevol } }),
	&((Keychord){1, {{ 0,                                XF86XK_MonBrightnessDown}},    spawn,                 {.v = decrbns } }),
	&((Keychord){1, {{ 0,                                XF86XK_MonBrightnessUp}},      spawn,                 {.v = incrbns } }),
	&((Keychord){1, {{ 0,                                XF86XK_Sleep}},                spawn,                 SHCMD("toggle-lock") }),
	/* { 0,                                XF86XK_AudioPlay,            spawn,                 {.v = { "sp", "play", NULL } } }, */
	/* { 0,                                XF86XK_AudioStop,            spawn,                 {.v = incrvol } }, */
	/* { 0,                                XF86XK_AudioNext,            spawn,                 {.v = incrvol } }, */
	/* { 0,                                XF86XK_AudioPrev,            spawn,                 {.v = incrvol } }, */
	TAGKEYS(                            XK_1,                        0)
	TAGKEYS(                            XK_2,                        1)
	TAGKEYS(                            XK_3,                        2)
	TAGKEYS(                            XK_4,                        3)
	TAGKEYS(                            XK_5,                        4)
	TAGKEYS(                            XK_6,                        5)
	TAGKEYS(                            XK_7,                        6)
	TAGKEYS(                            XK_8,                        7)
	TAGKEYS(                            XK_9,                        8)
    TAGKEYS(                            XK_KP_End,                   0)
    TAGKEYS(                            XK_KP_Down,                  1)
    TAGKEYS(                            XK_KP_Page_Down,             2)
    TAGKEYS(                            XK_KP_Left,                  3)
    TAGKEYS(                            XK_KP_Begin,                 4)
    TAGKEYS(                            XK_KP_Right,                 5)
    TAGKEYS(                            XK_KP_Home,                  6)
    TAGKEYS(                            XK_KP_Up,                    7)
    TAGKEYS(                            XK_KP_Page_Up,               8)
	/* &((Keychord){1, {{ MODKEY|ShiftMask,                 XK_q}},                        quit,                  {0} }), */
	&((Keychord){1, {{ MODKEY|ShiftMask,                 XK_r}},                        quit,                  {1} }),
	&((Keychord){1, {{ 0,                                HOLDKEY}},                     holdbar,               {0} }),
	&((Keychord){2, {{ MODKEY, XK_b},    {0, XK_b}},      spawn,      SHCMD("qutebrowser") }),
	&((Keychord){2, {{ MODKEY, XK_b},    {0, XK_o}},      spawn,      SHCMD("brave") }),
	&((Keychord){2, {{ MODKEY, XK_b},    {0, XK_f}},      spawn,      SHCMD("firefox") }),
	&((Keychord){2, {{ MODKEY, XK_b},    {0, XK_n}},      spawn,      SHCMD("nyxt") }),
	&((Keychord){2, {{ MODKEY, XK_b},    {0, XK_r}},      spawn,      SHCMD("st -e newsboat") }),
    /* Messaging */
	&((Keychord){2, {{ MODKEY, XK_m},    {0, XK_m}},      spawn,      SHCMD("thunderbird") }),
	&((Keychord){2, {{ MODKEY, XK_m},    {0, XK_t}},      spawn,      SHCMD("teams") }),
	&((Keychord){2, {{ MODKEY, XK_m},    {0, XK_n}},      spawn,      SHCMD("neomutt-wrapper") }),
    /* file explorer */
	&((Keychord){2, {{ MODKEY, XK_e},    {0, XK_e}},      spawn,      SHCMD("st -e lfub") }),
	&((Keychord){2, {{ MODKEY, XK_e},    {0, XK_p}},      spawn,      SHCMD("pcmanfm") }),
    /* dmenu scripts */
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_a}},      spawn,      SHCMD("dm-anicli") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_b}},      spawn,      SHCMD("dm-bookmarks") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_c}},      spawn,      SHCMD("dm-cal") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_e}},      spawn,      SHCMD("dm-emoji") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_g}},      spawn,      SHCMD("dm-games") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_h}},      spawn,      SHCMD("dm-hub") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_i}},      spawn,      SHCMD("dm-imdb") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_k}},      spawn,      SHCMD("dm-kill") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_l}},      spawn,      SHCMD("dm-logout") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_m}},      spawn,      SHCMD("dm-mail") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_n}},      spawn,      SHCMD("dm-net") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {ShiftMask, XK_n}},  spawn,  SHCMD("dm-note") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_p}},      spawn,      SHCMD("dm-pass") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {ShiftMask, XK_p}},  spawn,  SHCMD("dm-printer") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_q}},      spawn,      SHCMD("dm-radio") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_s}},      spawn,      SHCMD("dm-sounds") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {ShiftMask, XK_s}},  spawn,  SHCMD("dm-scan") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_t}},      spawn,      SHCMD("dm-todo") }),
	&((Keychord){2, {{ MODKEY, XK_p},    {0, XK_y}},      spawn,      SHCMD("dm-youtube") }),
    /* Print Screen */
	&((Keychord){1, {{ MODKEY|ControlMask, XK_Print}},    spawn,      SHCMD("dm-shot") }),
	&((Keychord){1, {{ MODKEY|ShiftMask, XK_Print}},      spawn,      SHCMD("flameshot gui") }),
	&((Keychord){1, {{ 0,                XK_Print}},      spawn,      SHCMD("flameshot screen") }),
    /* change wallpaper*/
	&((Keychord){1, {{ MODKEY|ControlMask, XK_w}},        spawn,      SHCMD("change-wallpaper") }),
    /* miscellaneous */
	&((Keychord){1, {{ MODKEY,             XK_Menu}},     spawn,      SHCMD("curriculum") }),
	&((Keychord){1, {{ MODKEY,             XK_v}},        spawn,      SHCMD("keepassxc") }),
	&((Keychord){1, {{ MODKEY,             XK_t}},        spawn,      SHCMD("st -t float -e toipe -n 50 -f $HOME/.config/toipe/english.txt") }),
	&((Keychord){1, {{ MODKEY,             XK_KP_Add}},   spawn,      SHCMD("qalculate-gtk --title=float") }),
};
	/* { MODKEY,                       XK_Tab,    view,           {0} }, */
	/* { MODKEY,                           XK_t,                        setlayout,             {.v = &layouts[0]} }, */
	/* { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} }, */
	/* { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} }, */
	/* { MODKEY,                       XK_space,  setlayout,      {0} }, */
	/* { MODKEY,                       XK_0,      view,           {.ui = ~0 } }, */
	/* { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } }, */
