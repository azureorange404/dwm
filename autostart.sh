#!/bin/bash

# sxhkd &
# makropadx enable &
numlockx on &
xset s 0 0 s noblank -dpms &
dunst -conf "$XDG_CONFIG_HOME"/dunst/dunstrc-dwm &
# feh --no-fehbg --randomize --bg-fill $XDG_DATA_HOME/wallpapers/paintings &
xwallpaper --zoom "$XDG_DATA_HOME/wallpapers/anime/"$(ls "$XDG_DATA_HOME/wallpapers/anime/" | shuf | head -n1)& 
systemctl --user enable batnotify.timer && systemctl --user start batnotify.timer &
    
# xautolock -corners 0-00 -cornersize 20 \
xautolock -detectsleep -time 3 -locker slock-wrapper &
          # -notify 30 -notifier "notify-send -h int:transient:1 -u critical -t 10000 -- 'LOCKING screen in 30 seconds'" &

xss-lock -- slock-wrapper -n &
localsend-daemon &
nextcloud --background &

picom &
