/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 0;        /* horizontal padding for statusbar */
static const int vertpadbar         = 6;        /* vertical padding for statusbar */
static const char *fonts[]          = { "monospace:size=10", "Regular Nerd Font Complete Mono:pixelsize=20" };
static const char dmenufont[]       = "monospace:size=10";
static unsigned int baralpha        = 0xd0;
static unsigned int borderalpha     = OPAQUE;
static const char col_gray1[]       = "#222222"; /* darkest gray */
static const char col_gray2[]       = "#2b2b2b";
static const char col_gray3[]       = "#444444";
static const char col_gray4[]       = "#bbbbbb";
static const char col_gray5[]       = "#eeeeee"; /* lightest gray */
static const char col_cyan[]        = "#005577";
static const char col_darkslategray[]  = "#255957"; /* grayish cyan */
static const char col_redwood[]     = "#A63D40"; /* pleasant red */
static const char col_cordovan[]    = "#A63D40"; /* dark red */
static const char col_black_bean[]  = "#3B1517"; /* super dark red */
static const char col_goldmetallic[]   = "#DDB84A"; /* nice yellow */
static const char col1[]            = "#ffffff";
static const char col2[]            = "#ffffff";
static const char col3[]            = "#ffffff";
static const char col4[]            = "#ffffff";
static const char col5[]            = "#ffffff";
static const char col6[]            = "#ffffff";

enum { SchemeNorm, SchemeCol1, SchemeCol2, SchemeCol3, SchemeCol4,
       SchemeCol5, SchemeCol6, SchemeSel }; /* color schemes */

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]  = { col_gray4, col_gray1, col_gray3 },
	[SchemeCol1]  = { col1,      col_gray1, col_gray2 },
	[SchemeCol2]  = { col2,      col_gray1, col_gray2 },
	[SchemeCol3]  = { col3,      col_gray1, col_gray2 },
	[SchemeCol4]  = { col4,      col_gray1, col_gray2 },
	[SchemeCol5]  = { col5,      col_gray1, col_gray2 },
	[SchemeCol6]  = { col6,      col_gray1, col_gray2 },
	[SchemeSel]   = { col_gray5, col_gray2,  col_redwood  },
};

static const char *const autostart[] = {
    "bash", "/home/azure/.local/source/suckless/dwm/autostart.sh", NULL,
    NULL /* terminate */
};

/* tagging */
static const char *tags[]    = { " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 " };
static const char *alttags[] = { "cmd", "www", "sys", "doc", "vrt", "cht", "med", "gfx", "vlt" }; /* ++alttagdecoration */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                      instance    title                tags mask     isfloating   monitor */
	{ "Brave-browser",            NULL,       NULL,                     1 << 1,       0,           -1 },
	{ "qutebrowser",              NULL,       NULL,                     1 << 1,       0,           -1 },
	{ NULL,                       NULL,       "LibreOffice",            1 << 3,       0,           -1 },
	{ "libreoffice-writer",       NULL,       NULL,                     1 << 3,       0,           -1 },
	{ "libreoffice-calc",         NULL,       NULL,                     1 << 3,       0,           -1 },
	{ "libreoffice-impress",      NULL,       NULL,                     1 << 3,       0,           -1 },
    { "Virt-manager",             NULL,       NULL,                     1 << 4,       0,           -1 },
	{ "thunderbird",              NULL,       NULL,                     1 << 5,       0,           -1 },
	{ "teams-for-linux",          NULL,       NULL,                     1 << 5,       0,           -1 },
	{ "firefox",                  NULL,       NULL,                     1 << 6,       0,           -1 },
	{ "firefox",                  NULL,       "Picture-in-Picture",     0,            1,           -1 },
	{ "Gimp",                     NULL,       NULL,                     1 << 7,       0,           -1 },
	{ "KeePassXC",                NULL,       NULL,                     1 << 8,       0,           -1 },
	{ "Localsend_app",            NULL,       NULL,                     0,            1,           -1 },
	{ "mpv",                      NULL,       NULL,                     0,            1,           -1 },
	{ NULL,                       NULL,       "float",                  0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[0]",      NULL },    /* no layout function means floating behavior */
	{ "[F]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
       &((Keychord){1, {{MODKEY, KEY}},                                        view,           {.ui = 1 << TAG} }), \
       &((Keychord){1, {{MODKEY|ControlMask, KEY}},                            toggleview,     {.ui = 1 << TAG} }), \
       &((Keychord){1, {{MODKEY|ShiftMask, KEY}},                              tag,            {.ui = 1 << TAG} }), \
       &((Keychord){1, {{MODKEY|ControlMask|ShiftMask, KEY}},                  toggletag,      {.ui = 1 << TAG} }),
#define HOLDKEY 0xff67 // replace 0 with the keysym to activate holdbar

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu-run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray4, "-sb", col_darkslategray, "-sf", col_gray5, "-c", "-bw", "1", "-l", "20", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *quitcmd[]  = { "dm-logout", NULL };
static const char *chngebg[]  = { "feh", "--randomize", "--bg-fill", "/home/azure/.wallpapers/paintings", NULL };
static const char *decrvol[]  = { "bash", ".config/shell/scripts/dunst/volume.sh", "down", NULL };
static const char *incrvol[]  = { "bash", ".config/shell/scripts/dunst/volume.sh", "up", NULL };
static const char *mutevol[]  = { "bash", ".config/shell/scripts/dunst/volume.sh", "mute", NULL };
static const char *tgltray[]  = { "bash", ".config/shell/scripts/dwm/toggletrayer.sh", NULL };
static const char *decrbns[]  = { "bash", ".config/shell/scripts/dunst/brightness.sh", "down", NULL };
static const char *incrbns[]  = { "bash", ".config/shell/scripts/dunst/brightness.sh", "up", NULL };

#include "shift-tools.c"
#include "movestack.c"
#include <X11/XF86keysym.h>
#include "keybindings.h"

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

